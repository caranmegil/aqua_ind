# aqua_ind.py - Driver of querying service
# Copyright (C) 2021  William R. Moore <william@nerderium.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <https://www.gnu.org/licenses/>.

import serial
import requests
import time
from dotenv import load_dotenv
import os
import pidfile

load_dotenv()

set_point = float(os.environ.get('SET_POINT'))
sleep_timer = 1
executions = 30
tick_threshold = 15
ticks = 0
executions_ticks = 0

try:
    with pidfile.PIDFile():
        with serial.Serial('/dev/ttyACM0', 9600) as ser:
            if ser.isOpen():
                while executions_ticks < executions:
                    value = float(ser.readline())
                    if value < set_point:
                        ticks += 1
                        executions_ticks = 0
                    else:
                        ticks = 0

                    if ticks == tick_threshold:
                        ticks = 0
                        print(requests.post(os.environ.get('NOTIF_URL'), json={'message': os.environ.get('MESSAGE')}))
                    executions_ticks += 1
                    time.sleep(sleep_timer)
except pidfile.AlreadyRunningError:
    print('It is already running!')
